#!/usr/bin/env bash

mkdir -p $HOME/bin

# add local bin to path if it is not already
case :$PATH:
  in *:$HOME/bin:*) ;;
     *) echo "export PATH=\$PATH:$HOME/bin" >> $HOME/.bash_profile;;
esac

# compile and install zsh
wget http://www.zsh.org/pub/zsh-5.7.1.tar.xz
tar xf zsh-5.7.1.tar.xz
cd zsh-5.7.1/
./configure --prefix=$HOME --bindir=$HOME/bin
make
make install
cd ..
rm -rf zsh-5.7.1 zsh-5.7.1.tar.xz

# start zsh on login
echo "exec \$HOME/bin/zsh -l" >> $HOME/.bash_profile

# install oh-my-zsh and configs
if [[ ! -d ~/.oh-my-zsh ]]; then
    git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh/
fi

cd ~/.oh-my-zsh/custom/plugins
git clone https://github.com/zdharma/fast-syntax-highlighting.git
git clone https://github.com/zsh-users/zsh-autosuggestions

# download zshrc
curl https://gitlab.com/jhring/vacc_zsh/raw/master/zshrc -o $HOME/.zshrc

echo "Install complete, changes will take effect on next login."
