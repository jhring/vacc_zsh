# VACC ZSH
This is a simple self contained script to install and configure ZSH without root privileges.
It was written for and tested on the Vermont Advanced Computing Core (VACC) but should work on any UNIX-like system. 

## Install
To install or update simply run the following command. Note that this command will modify your dotfiles so take care.

```bash -c "$(curl -fsSL https://gitlab.com/jhring/vacc_zsh/raw/master/install_zsh.sh)"```
